$( document ).ready(function() {
    
});


var lastScrollTop = 0;
var mobileScrollTop = 0;



function toggleNav() {
    this.toggleClass('fullnav', 'fullnav-active');
}

function toggleClass(id, classname) {
    document.getElementById(id).classList.toggle(classname);
}

$(window).scroll(function (event) {
    var st = $(this).scrollTop();
    toggleNavbarScroll(st, lastScrollTop, 250);
    lastScrollTop = st;
});

function toggleNavbarScroll(st, comparator, maxScroll) {
    if (comparator < maxScroll) {
        document.getElementById('top-menu').classList.add('text-white');
        document.getElementById('mainNav').classList.remove('navbar-active');
        document.getElementById('mainNav').classList.remove('navbar-inactive');
        document.getElementById('right-menu').classList.add('hidden');
    } else if (st > comparator && st > maxScroll) {
        document.getElementById('top-menu').classList.remove('text-white');
        document.getElementById('mainNav').classList.remove('navbar-active');
        document.getElementById('mainNav').classList.add('navbar-inactive');
        document.getElementById('right-menu').classList.remove('hidden');
    } else {
        document.getElementById('mainNav').classList.add('navbar-active');
        document.getElementById('mainNav').classList.remove('navbar-inactive');
    }
}

function scrollToID(id) {
    document.querySelector(id).scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
    });
    document.getElementById('mainNav').classList.remove('navbar-active');
    document.getElementById('mainNav').classList.add('navbar-inactive');
}

function fullNavLink(id) {
    toggleNav();
    scrollToID(id);
}

/*
var typeout = document.getElementById('typeout');
var typewriter = new Typewriter(typeout, {
    loop: true,
    delay: 50
});
typewriter.typeString('is a Control System for large scale drone networks')
    .pauseFor(2500)
    .deleteAll(25).typeString('is a Universal Traffic Management solution')
    .pauseFor(2500)
    .deleteAll(25)
    .typeString('is an ultra-efficient aviation infrastructure super-highway')
    .pauseFor(2500)
    .deleteAll(25)
    .typeString('unlocks interoperable autonomy')
    .pauseFor(2500)
    .deleteAll(25)
    .typeString('is a Permissioned Distributed Ledger Technology')
    .pauseFor(2500)
    .deleteAll(25)
    .typeString('is decentralised & byzantine secure')
    .pauseFor(2500)
    .deleteAll(25)
    .typeString('is immune to data corruption')
    .pauseFor(2500)
    .deleteAll(25)
    .typeString('IOT meets Blockchain')
    .pauseFor(2500)
    .deleteAll(25)
    .start();
*/